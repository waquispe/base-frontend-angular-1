'use strict';

angular.module('baseApp')
  .controller('AdminHomeCtrl', function ($scope, $location, UsuarioAuthentication) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    // Inicializa los efectos material
    $.material.init();

    $scope.open_page = function(location) {
      $location.path(location);
    }

    $scope.logout = function() {
      UsuarioAuthentication.logout();
    }

  });
