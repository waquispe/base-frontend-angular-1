# Instalación

### 1. Clonar el proyecto

$ `git clone git@gitlab.com:aquispe/base-frontend-angular-1.git`

### 2. Entrar en el directorio del proyecto

$ `cd base-frontend-angular-1`

### 3. Instalar las dependencias

- $ `npm install` (npm)
- $ `bower install` (bower)

### 4. Construir la aplicación

$ `grunt build` (Producción)

#### Nota.-
Este comando crea la carpeta de producción `dist`.

### 5. Iniciar la aplicación

$ `grunt serve` (Desarrollo)

$ `grunt serve:dist` (Producción)

## Opciones adicionales

#### Ejecutar las pruebas unitarias [Karma]

$ `grunt test`

Para mas información sobre `yeoman` visita la pagina  [YEOMAN](http://yeoman.io/).
